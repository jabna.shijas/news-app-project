

const key = "pub_44311799b3a75996806c4623a3cf4dfe3a176";

const cardData = document.querySelector(".cardData");
const searchBtn = document.getElementById("searchBtn");
const inputData = document.getElementById("inputData");

const politicsBtn = document.getElementById("Politics")
const businessBtn = document.getElementById("Business");
const sportsBtn = document.getElementById("Sports");
const technologyBtn = document.getElementById("Technology");
const entertainmentBtn = document.getElementById("Entertainment");
const searchType = document.getElementById("type");
 


const getData = async (input) => {
    try {
      // const response = await fetch(`https://newsapi.org/v2/everything?q=${input}&apikey=${key}`);
        
      const response = await fetch(`https://newsdata.io/api/1/news?apikey=pub_44311799b3a75996806c4623a3cf4dfe3a176&q=${input}`);

        const data = await response.json();
        console .log(data)
       searchType.innerText = "Search : " + input;
        cardData.innerHTML = "";
        inputData.value = " ";

        data.results.forEach(function (results, index) {
            let divs = document.createElement("div");
            divs.classList.add("card");
            cardData.appendChild(divs);
            divs.innerHTML = `
                <img src="${results.image_url}"  alt="">
                <h4 class="card-title">${results.title}</h4>
                <p>${results.description}</p>
            `;

            const title = divs.querySelector(".card-title");
            title.addEventListener("click", function () {
                window.open(results.link);
            });
        });
    } catch (error) {
        console.error('An error occurred:', error.message);
        // Handle the error, display an error message, or perform other actions as needed
    }
};


window.addEventListener("load", function () {

    getData("Today's Headlines");

})



searchBtn.addEventListener("click", function () {
    let inputText = inputData.value;
    getData(inputText);

})

politicsBtn.addEventListener("click", function() {

    getData("Politics");

})
businessBtn.addEventListener("click", function () {

    getData("Business");

})

sportsBtn.addEventListener("click", function () {

    getData("Sports");

})
technologyBtn.addEventListener("click", function () {

    getData("Technology");

})

entertainmentBtn.addEventListener("click", function () {

    getData("Entertainment");

})
